import sys
import requests
import dropbox
import os
import time
import logging
from subprocess import call

# /*
#  * 1. fetch a queue entry
#  * 2. download s3link
#  * 3. upload to dbox
#  * 4. make entry in bundle
#  * 5. delete s3link
#  * 6. delete entry in queue
#  */


host = 'https://www-orangestor.rhcloud.com/api'

subq = sys.argv[1]
secret = sys.argv[2]

logger = logging.getLogger(subq)
logger.setLevel(logging.INFO)

handler = logging.FileHandler('../logs/' + subq + '.log')
handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


while 1:
	try:
		r = requests.get(host + '/uploadq/' + secret + '/' + subq)
	except Exception as ex:
		logger.error("Error on request.get(" + "/uploadq/$secret/" + subq + ")", exc_info=True)
		time.sleep(5)
		continue
	if r.status_code != 200:
		print "Sleeping for 5 secs"
		time.sleep(5)
		continue

	quentry = r.json()
	try:
		s3filename = quentry['s3filename']
		filename = quentry['filename']
		dbtoken = quentry['dbtoken']
		ticketid = str(quentry['ticketid'])
		usertoken = quentry['usertoken']
	except:
		logger.error("Unable to get quentry['']", exc_info=True)

	s3link = "https://orangestor.s3.amazonaws.com/" + s3filename
	logger.info("Processing s3filename : " + s3filename)

	retval = call (["s3curl.pl", "--id", "orangestor", "--", "-s", "-o", s3filename, s3link])
	if retval != 0:
		logger.error("Unable to download : " + s3filename)
		continue

	dbclient = dropbox.client.DropboxClient(dbtoken)
	dirpath = "/" + ticketid
	try:
		dbclient.file_create_folder(dirpath)
	except dropbox.rest.ErrorResponse as ex:
		if ex.status != 403:
			logger.error("Error creating dir " + dirpath + " " + ex.error_msg, exc_info=True)
			continue

	bigFile = open(s3filename, 'rb')
	size = os.path.getsize(s3filename)
	uploader = dbclient.get_chunked_uploader(bigFile, size)
	while uploader.offset < size:
	    try:
	        upload = uploader.upload_chunked()
	    except rest.ErrorResponse as ex:
	    	logger.error("Error during upload of : " + s3filename + " : " + ex.error_msg, exc_info=True)
	    	continue

	try:
		uploader.finish(dirpath + '/' + filename)
	except rest.ErrorResponse as ex:
		logger.error("uploader.finish() error : " + ex.error_msg, exc_info=True)
		continue

	try:
		dblinkdict = dbclient.share(dirpath + '/' + filename, False)
	except rest.ErrorResponse as ex:
		logger.error("Error in dbclient.share(" + dirpath + '/' + filename + ") " + "s3filename: " + s3filename, exc_info=True)
		continue

	payload = {'usertoken': usertoken, 'ticketid': ticketid, 's3filename': s3filename, 'dblink': dblinkdict['url']}
	r = requests.put(host + '/bundle/dbuploaded/' + secret, data=payload)
	if r.status_code != 200:
		logger.error("Unable to do dbuploaded for " + s3filename)
		continue

	r = requests.delete(host + '/uploadq/' + secret + '/' + quentry['_id'])
	if r.status_code != 204:
		logger.error("Unable to delete in uploadq : " + s3filename)
		continue

	retval = call (["s3curl.pl", "--id", "orangestor", "--delete", "--", s3link])
	if retval != 0:
		logger.error("Unable to delete : " + s3link)
		continue

	try:
		os.remove(s3filename)
	except:
		logger.error("Unable to delete : " + s3filename, exc_info=True)

