'use strict';

angular.module('orangestorApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'satellizer',
  'mgcrea.ngStrap',
  'angularSpinner'
])
.config(function ($routeProvider, $locationProvider, $authProvider) {
  $routeProvider
    .otherwise({
      redirectTo: '/'
    });

  $authProvider.loginRedirect = '/home';
  $authProvider.loginUrl = '/api/auth/login';
  $authProvider.signupUrl = '/api/auth/signup';
  $authProvider.loginOnSignup = false;
  $authProvider.signupRedirect = '/signup';

  $locationProvider.html5Mode(true);
});
