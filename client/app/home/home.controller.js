'use strict';

angular.module('orangestorApp')
  .controller('HomeCtrl', function ($scope, $auth, $location, $http) {
    $scope.id = '{{id}}';
    $scope.request_details = '{{request_details}}';
    $scope.submit_request = '{{submit_request}}';
    if (!$auth.isAuthenticated()) {
    	$location.path('/');
    }

    $http.get('/api/auth/profile').
    then(function(data) {
        $scope.profile = data.data;
    }, function(data) {
        console.log("error getting profile info");
    });

    $scope.dbauth = function() {
    	$http.get('/api/auth/dbauth').
    	then(function(data){
    		window.location = data.data;
    	}, function(data){
    		console.log("error in redirect")
    		console.log(data);
    	});
    }
  });
