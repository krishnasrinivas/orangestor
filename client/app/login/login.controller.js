'use strict';

angular.module('orangestorApp')
  .controller('LoginCtrl', function ($scope, $auth, $alert, usSpinnerService) {
  	window.auth = $auth;
  	$scope.login = function() {
  		usSpinnerService.spin('spinner-1');
  		var a = $auth.login({
  			email: $scope.email,
  			password: $scope.password
  		}).then(function success(data, status) {
  			usSpinnerService.stop('spinner-1');
  		}, function error(data, status) {
  			usSpinnerService.stop('spinner-1');
  			$alert({title: 'Login fail : ', content: data.data.message, placement: 'top', type: 'info', show: true, container: "#alertcontainer"});
  		})
  	}
  });
