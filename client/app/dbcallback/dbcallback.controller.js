'use strict';

angular.module('orangestorApp')
  .controller('DbcallbackCtrl', function ($scope, $routeParams, $http, $location, usSpinnerService) {
    $scope.message = 'Hello';
    // $http.get('/api/auth/dbauth/callback', {params: {
    // 	state: $routeParams.state,
    // 	code: $routeParams.code}
    // }).then(function(data) {
    // 	console.log(data);
    // }, function(data) {
    // 	console.log(data);
    // })
    usSpinnerService.spin('spinner-3');
    $http({
        url: '/api/auth/dbauth/callback',
        params: {
            state: $routeParams.state,
            code: $routeParams.code
        }
    }).success(function() {
        usSpinnerService.stop('spinner-3');
        $location.url('/home');
    }).error(function() {
        usSpinnerService.stop('spinner-3');
        $location.path('/home');
    })
  });
