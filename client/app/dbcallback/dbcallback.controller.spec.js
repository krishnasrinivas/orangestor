'use strict';

describe('Controller: DbcallbackCtrl', function () {

  // load the controller's module
  beforeEach(module('orangestorApp'));

  var DbcallbackCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DbcallbackCtrl = $controller('DbcallbackCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
