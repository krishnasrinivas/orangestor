'use strict';

angular.module('orangestorApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/dbcallback', {
        templateUrl: 'app/dbcallback/dbcallback.html',
        controller: 'DbcallbackCtrl'
      });
  });
