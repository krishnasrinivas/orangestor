'use strict';

angular.module('orangestorApp')
  .controller('VerifyCtrl', function ($scope, $routeParams, $http, $alert) {
  	$http.get('/api/auth/verify/' + $routeParams.email + '/' + $routeParams.verifytoken)
  	.then(function(data, status) {
  		$alert({title: 'Email verified : ', content: "Please login", placement: 'top', type: 'info', show: true, container: "#alertcontainer"});
  	}, function(data, status) {
  		$alert({title: 'Email verified failed: ', content: "token expired", placement: 'top', type: 'info', show: true, container: "#alertcontainer"});
  	});
  });
