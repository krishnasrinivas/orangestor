'use strict';

angular.module('orangestorApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/verify/:email/:verifytoken', {
        templateUrl: 'app/verify/verify.html',
        controller: 'VerifyCtrl'
      });
  });
