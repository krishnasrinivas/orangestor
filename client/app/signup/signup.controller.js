'use strict';

angular.module('orangestorApp')
  .controller('SignupCtrl', function ($scope, $auth, usSpinnerService, $alert) {
  	$scope.signup = function() {
  		usSpinnerService.spin('spinner-2');
  		$auth.signup ({
  			email: $scope.email,
  			displayName: $scope.displayName,
  			password: $scope.password
  		}).then(function(data, status) {
  			usSpinnerService.stop('spinner-2');
        $('form').css('display', 'none');
        $alert({title: 'Verify email : ', content: "Please check your email to verify your account", placement: 'top', type: 'info', show: true, container: ".panel-body"});
  		}, function(data, status) {
  			usSpinnerService.stop('spinner-2');
  			$alert({title: 'Signup fail : ', content: data.data.err, placement: 'top', type: 'info', show: true, container: "form"});
  		});
  	}
  });
