function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var user = getParameterByName('user');
var ticketid = getParameterByName('ticketid');
var files = [];

var spinnercontainer = document.getElementById("spinnercontainer");
var opts = {
    lines:11,
    length:5,
    width:3,
    radius:8,
    corners:1.0,
    rotate:0,
    trail:60,
    speed:1.0,
    direction:1
}
var spinner = new Spinner(opts).spin(spinnercontainer);

function refreshfilelist() {
    if (!user || !ticketid) {
        console.log("user/ticketid not specified");
        return;
    }
    $.get('/api/bundle/user/' + user + '/' + ticketid, function(data, status) {
        var innerhtml = "";
        if (!data)
            return;
        files = data;
        data = data.reverse();
        for (var i = 0; i < data.length; i++) {
            innerhtml = innerhtml +
                '<li class="clearfix"><div class="filebox-body clearfix">' + data[i].filename + '</div></li>'
        }
        $("#orangestor-box").html(innerhtml);
    });
}
refreshfilelist();

function onchange() {
    var _uploaderr = false;
    var filename;

    if (!$("#orangestor-input")[0].files.length)
        return;

    function uploaderror(event) {
        _uploaderr = true;
    }

    function uploadprogressupdate(event) {
        if (event.lengthComputable) {
            var percent = event.loaded / event.total * 100;
            $('.progress-bar').css('width', Math.floor(percent) + '%');
            $('.progress-bar').text(Math.floor(percent) + '%');
            if (Math.floor(percent) === 100)
                $('.progress-bar').text(Math.floor(percent) + '%');
        }
    }

    if (!$("#orangestor-input")[0].files.length)
        return;

    if ($("#orangestor-input")[0].files[0].size > 1024 * 1024 * 1024) {
        alert("File size should be less than 1G");
        return;
    }
    filename = $("#orangestor-input")[0].files[0].name;

    for (var i = 0; i < files.length; i++) {
        if (filename === files[i].filename) {
            var outer = document.getElementById("orangestor-input-outer");
            outer.innerHTML = outer.innerHTML;
            setTimeout(function() {
                $("#orangestor-input").change(onchange);
            },100);
            alert("Error : File with same name already uploaded");
            return;
        }
    }

    $('#progress-outer').show();

    $.get('/api/uploadq/s3upload/' + user, {
        filename: filename
    }, function(data, status) {
        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        fd.append('key', data.key);
        fd.append('AWSAccessKeyId', data.AWSAccessKeyId);
        fd.append('acl', data.acl);
        //fd.append('success_action_redirect', data.success_action_redirect);
        fd.append('policy', data.policy);
        fd.append('signature', data.signature);
        fd.append('Content-Type', data.ContentType);

        // This file object is retrieved from a file input.
        fd.append('file', $("#orangestor-input")[0].files[0]);

        xhr.upload.addEventListener("error", uploaderror);
        xhr.upload.addEventListener("progress", uploadprogressupdate);
        // xhr.upload.addEventListener("readystatechange", uploadreadystatechange);
        xhr.open('POST', 'https://orangestor.s3.amazonaws.com/', true);

        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) {
                return;
            }
            if (_uploaderr) {
                $('#progress-outer').hide();
                alert("Error uploading, try again");
            } else {
                $.ajax({
                    url: '/api/uploadq',
                    type: 'PUT',
                    data: {
                        usertoken: user,
                        ticketid: ticketid,
                        s3filename: data.key,
                        filename: filename
                    },
                    success: function(data) {
                        refreshfilelist();
                        var outer = document.getElementById("orangestor-input-outer");
                        outer.innerHTML = outer.innerHTML;
                        setTimeout(function() {
                            $("#orangestor-input").change(onchange);
                        },100);
                        $('#progress-outer').hide();
                    },
                    error: function(data) {
                        refreshfilelist();
                        var outer = document.getElementById("orangestor-input-outer");
                        outer.innerHTML = outer.innerHTML;
                        setTimeout(function() {
                            $("#orangestor-input").change(onchange);
                        },100);
                        $('#progress-outer').hide();
                        alert("Error during upload");
                    }
                });
            }
        };
        $('.progress-bar').css('width', '0%');
        xhr.send(fd);
    });
}

$("#orangestor-input").change(onchange);
