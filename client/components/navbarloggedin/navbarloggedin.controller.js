'use strict';

angular.module('orangestorApp')
  .controller('NavbarLoggedinCtrl', function ($scope, $location, $auth) {
    $scope.isCollapsed = true;
    $scope.displayName = "Kris";

    if (!$auth.isAuthenticated()) {
      $location.path('/');
    }

    $scope.logout = function() {
      $auth.logout();
    }
    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });