'use strict';

angular.module('orangestorApp')
  .controller('NavbarCtrl', function ($scope, $location, $auth) {
    $scope.isCollapsed = true;
    if ($auth.isAuthenticated()) {
    	$location.path('/home');
    }

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });