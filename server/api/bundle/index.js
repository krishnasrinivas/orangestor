'use strict';

var express = require('express');
var controller = require('./bundle.controller');

var router = express.Router();

/* user add file - put usertoken+id+dropboxfilepath : python upload script */

/* user get bundle - get usertoken+id */
/* agent get bundle - get usertoken+id */

var s3todbsecret = process.env.S3TODBSECRET;

router.get('/agent/:agenttoken/:ticketid', controller.agentshowbundle);
router.get('/user/:usertoken/:ticketid', controller.usershowbundle);
router.put('/dbuploaded/' + s3todbsecret, controller.dbuploaded);

module.exports = router;