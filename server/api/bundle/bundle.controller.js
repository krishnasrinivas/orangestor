'use strict';

var _ = require('lodash');
var Bundle = require('./bundle.model');
var User = require('../auth/auth.model');

// Get a single bundle
exports.agentshowbundle = function(req, res) {
  User.findOneQ({agenttoken: req.params.agenttoken})
    .then(function(user) {
      if (!user)
        throw new Error("User not found");
      return Bundle.findOneQ({bundleowner: user.bundleowner, ticketid: req.params.ticketid});
    })
    .then(function(bundle) {
      if (!bundle)
        throw new Error("Bundle not found");
      return res.send(bundle.files);
    })
    .catch(function(e) {
      return res.send(404);
    })
};

exports.usershowbundle = function(req, res) {
  User.findOneQ({usertoken: req.params.usertoken})
    .then(function(user) {
      if (!user)
        throw new Error("User not found");
      return Bundle.findOneQ({bundleowner: user.bundleowner, ticketid: req.params.ticketid})
    })
    .then(function(bundle) {
      if(!bundle)
        throw new Error("User not found");
      for (var i = 0; i < bundle.files.length; i++) {
        bundle.files[i].s3filename = "";
        bundle.files[i].dblink = "";
        bundle.files[i].s3downloadurl = "";
      }
      return res.json(bundle.files);
    })
    .catch(function(e) {
      return res.send(404);
    })
};

exports.dbuploaded = function(req, res) {
  User.findOneQ({usertoken: req.body.usertoken})
    .then(function(user) {
      if (!user)
        throw new Error("User not found");
      return Bundle.updateQ({bundleowner: user.bundleowner,
                  ticketid: req.body.ticketid,
                  "files.s3filename": req.body.s3filename},
                  { $set: { "files.$.dbuploaded" : true,
                    "files.$.dblink": req.body.dblink }});
    })
    .then(function(bundle) {
      if (!bundle)
        throw new Error("Bundle not found");
      return res.send(200);
    })
    .catch(function(e) {
      return res.send(404)
    });
}
