'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema;

var BundleSchema = new Schema({
	bundleowner: String,
	ticketid: Number,
	files:[{filename: String, dblink: String, s3filename: String, dbuploaded: Boolean, date: Date, s3downloadurl: String}]
});

BundleSchema.index({bundleowner: 1, ticketid: 1});

module.exports = mongoose.model('Bundle', BundleSchema);
