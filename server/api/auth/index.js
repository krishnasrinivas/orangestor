'use strict';

var express = require('express');
var controller = require('./auth.controller');

var router = express.Router();

router.post('/signup', controller.signup);
router.post('/login', controller.login);
router.get('/profile', controller.profile);
router.get('/verify/:email/:verifytoken', controller.verify);
router.get('/dbauth/callback', controller.dbauthcallback);
router.get('/dbauth', controller.dbauth);

module.exports = router;