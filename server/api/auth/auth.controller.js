'use strict';

var moment = require('moment');
var jwt = require('jwt-simple');
var uuid = require('node-uuid');
var crypto = require('crypto');
var request = require('request');
var url = require('url');

var _ = require('lodash');
var User = require('./auth.model');

var mailgun_api_key = process.env.MAILGUN_API_KEY;
var mailgun_domain = 'orangestor.com';
var mailgun = require('mailgun-js')({apiKey: mailgun_api_key, domain: mailgun_domain});

var dbappkey = process.env.DBAPPKEY;
var dbappsecret = process.env.DBAPPSECRET;

var config = {
  TOKEN_SECRET: process.env.TOKEN_SECRET
}

function generateCSRFToken() {
    return crypto.randomBytes(18).toString('base64')
        .replace(/\//g, '-').replace(/\+/g, '_');
}

function generateRedirectURI(req) {
    return url.format({
            protocol: 'https',
            host: req.headers.host,
            pathname: '/dbcallback'
    });
}

function ensureAuthenticated(req) {
  if (!req.headers.authorization) {
    throw new Error("Not authenticated")
  }

  var token = req.headers.authorization.split(' ')[1];
  var payload; 
  payload = jwt.decode(token, config.TOKEN_SECRET);

  if (payload.exp <= Date.now()) {
    throw new Error("Token has expired");
  }
  req.user = payload.userid;
}

function createToken(req, user) {
  var payload = {
    userid: user._id,
    exp: moment().add(14, 'days').valueOf()
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
}

exports.signup = function(req, res) {
  var user = new User();
  user.displayName = req.body.displayName;
  user.email = req.body.email;
  user.password = req.body.password;
  user.verified = false;
  user.verifytoken = uuid.v4();
  user.agenttoken = uuid.v4().replace(/-/g, '');
  user.usertoken = uuid.v4().replace(/-/g, '');
  user.bundleowner = uuid.v4().replace(/-/g, '');

  user.saveQ()
    .then(function(_user) {
      var data = {
        from: 'OrangeStor <no-reply@orangestor.com>',
        to: user.email,
        subject: 'OrangeStor: verify email address',
        text: 'Please click this link to verify email address:\n' +
              'https://www.orangestor.com/verify/' + user._id + '/' + user.verifytoken + ' \n'
      };

      mailgun.messages().send(data, function (error, body) {
        if (error) {
          console.log("Error sending verify email to " + user.email);
        }
      });
      res.send({ token: createToken(req, user) });
    })
    .catch(function(e) {
      res.send(500);
    });
}

exports.profile = function(req, res) {
  try {
    ensureAuthenticated(req);
  } catch (e) {
    return res.send(401, e.message);
  }
  User.findByIdQ(req.user)
    .then(function(_user) {
      if (!_user)
        throw new Error("User not found");
      res.send({
        displayName: _user.displayName,
        email: _user.email,
        agenttoken: _user.agenttoken,
        usertoken: _user.usertoken,
        dbauthenticated: _user.dbtoken? 1:0
      });
    }).catch(function(e) {
      return res.send(500, {message: e.message});
    });
}

exports.login = function(req, res) {
  User.findOneQ({ email: req.body.email }, '+password')
    .then(function(_user) {
      if (!_user)
        throw new "Wrong email and/or password";
      if (!_user.verified)
        throw new "Please verify your email (you should have got a verification email after signing-up)"
      if (!_user.comparePassword(req.body.password))
        throw new "Wrong email and/or password";
      return res.send({ token: createToken(req, _user) });
    })
    .catch(function(e) {
      res.send(401, {message: e.message});
    });
}

exports.verify = function(req, res) {
  User.findByIdQ(req.params.email)
    .then(function(_user) {
      if (!_user)
        throw new "Token expired";
      if (_user.verifytoken !== req.params.verifytoken)
        throw new "Token expired";
      _user.verified = true;
      return _user.saveQ();
    })
    .then (function(_user) {
      return res.send(200, {});
    })
    .catch(function(e) {
      return res.send(401, {message: e.message});
    });
}

exports.dbauth = function(req, res) {
  try {
    ensureAuthenticated(req);
  } catch (e) {
    return res.send(401, e.message);
  }

  var csrfToken = generateCSRFToken();
  res.cookie('csrf', csrfToken);
  res.send(url.format({
      protocol: 'https',
      hostname: 'www.dropbox.com',
      pathname: '1/oauth2/authorize',
      query: {
          client_id: dbappkey,
          response_type: 'code',
          state: csrfToken,
          redirect_uri: generateRedirectURI(req)
      }
  }));
}

exports.dbauthcallback = function(req, res) {
  try {
    ensureAuthenticated(req);
  } catch (e) {
    return res.send(401, e.message);
  }

  if (req.query.error) {
      return res.send(500, 'ERROR ' + req.query.error + ': ' + req.query.error_description);
  }

  if (req.query.state !== req.cookies.csrf) {
      return res.status(500).send(
          'CSRF token mismatch, possible cross-site request forgery attempt.'
      );
  }
  // exchange access code for bearer token
  request.post('https://api.dropbox.com/1/oauth2/token', {
      form: {
          code: req.query.code,
          grant_type: 'authorization_code',
          redirect_uri: generateRedirectURI(req),
          client_id: dbappkey,
          client_secret: dbappsecret,
      }
  }, function (error, response, body) {
      try {
        var data = JSON.parse(body);
      } catch (e) {
        return res.send(500, "JSON.parse() error");
      }

      if (data.error) {
          return res.status(500).send('ERROR: ' + data.error);
      }

      // extract bearer token
      var token = data.access_token;

      User.findByIdQ(req.user)
        .then(function(_user) {
          _user.dbtoken = token;
          return _user.saveQ();
        })
        .then(function(_user) {
          return res.redirect('/home');
        })
        .catch(function(e) {
          return res.send(500, "Unable to fund user");
        });
  });
}
