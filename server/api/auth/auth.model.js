'use strict';

var bcrypt = require('bcryptjs');

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
  email: { type: String, unique: true, lowercase: true, index: true },
  password: { type: String, select: false },
  displayName: String,
  verified: Boolean,
  verifytoken: String,
  dbtoken: String,
  agenttoken: { type: String, unique: true, index: true},
  usertoken: { type: String, unique: true, index: true},
  bundleowner: String
});

UserSchema.pre('save', function(next) {
  var user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err, hash) {
      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function(password, done) {
  return bcrypt.compareSync(password, this.password)
};

module.exports = mongoose.model('Auth', UserSchema);
