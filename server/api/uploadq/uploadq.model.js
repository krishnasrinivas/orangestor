'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema;

var UploadqSchema = new Schema({
	usertoken: String,
	ticketid: Number,
	s3filename: String,
	filename: String,
	tries: Number
});

module.exports = mongoose.model('uploadq', UploadqSchema);
