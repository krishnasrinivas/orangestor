'use strict';

var _ = require('lodash');
var Uploadq = require('./uploadq.model');
var uuid = require('node-uuid');
var CryptoJS = require("crypto-js");
var User = require('../auth/auth.model');
var Bundle = require('../bundle/bundle.model');

// Get a single uploadq
exports.show = function(req, res) {
  var uploadq;
  var user;
  Uploadq.findOneQ({ticketid:{$mod:[10, req.params.subq]}, tries: {$lt:3}})
    .then(function(_uploadq) {
      if (!_uploadq)
        throw new Error("Uploadq not found");
      uploadq = _uploadq;
      return User.findOneQ({usertoken:uploadq.usertoken});
    })
    .then(function(_user) {
      if (!_user)
        throw new Error("User not found");
      user = _user;
      uploadq.tries = uploadq.tries + 1;
      return uploadq.saveQ();
    })
    .then(function(_uploadq)) {
      if (!_uploadq)
        throw new Error("Uploadq not found");
      var retjson = JSON.parse(JSON.stringify(_uploadq));
      retjson.dbtoken = user.dbtoken;
      return res.json(retjson);
    })
    .catch(function(e) {
      return res.send(404);
    });
}

// Creates a new uploadq in the DB.
exports.create = function(req, res) {

  var ContentMD5 = "";
  var ContentType = "";
  var Expires;
  var expirytime = new Date();
  expirytime.setSeconds(60*60*24*3);
  Expires = Math.floor(expirytime.getTime() / 1000);
  var StringToSign = "GET" + "\n" +
      ContentMD5 + "\n" +
      ContentType + "\n" +
      Expires + "\n" +
      "/orangestor/" + req.body.s3filename +
      "?response-content-disposition=attachment;filename=\"" + req.body.filename + "\"";
  var awssecret = process.env.AWSSECRET;
  var AWSAccessKeyId = process.env.AWSACCESSKEYID;

  var signature = CryptoJS.HmacSHA1(StringToSign, awssecret).toString(CryptoJS.enc.Base64);

  var s3downloadurl = "https://orangestor.s3.amazonaws.com/" + req.body.s3filename + "?response-content-disposition=" +  "attachment;filename=" + encodeURIComponent('"' + req.body.filename + '"') + "&AWSAccessKeyId=" + AWSAccessKeyId + "&Expires=" + Expires + "&Signature=" + encodeURIComponent(signature);

  User.findOne({usertoken: req.body.usertoken}, function(err, user) {
    if(err) { return handleError(res, err); }
    if (!user) {
      return res.send(404);
    }
    var entry = req.body;
    entry.tries = 0;
    Uploadq.create(entry, function(err, uploadq) {
      if(err) { return handleError(res, err); }
      Bundle.update({bundleowner: user.bundleowner, ticketid: req.body.ticketid}, {
        $set: {bundleowner: user.bundleowner,
        ticketid: req.body.ticketid,},
        $addToSet: {files:{filename: req.body.filename, s3filename: req.body.s3filename, dbuploaded: false, date: new Date, s3downloadurl: s3downloadurl}}
      }, {upsert: true}, function(err, bundle) {
        if (err) { return handleError(res, err);}
        return res.send(200);
      });
    });
  });
};

// Deletes uploadq entry from the DB.
exports.destroy = function(req, res) {
  Uploadq.findByIdAndRemoveQ(req.params.id)
    .then(function() {
      return res.send(204);
    })
    .catch(function(e) {
      return res.send(404);
    });
}

exports.s3upload = function(req, res) {
  User.findOne({usertoken: req.params.usertoken}, function(err, user) {
    if(err) { return handleError(res, err); }
    if (!user) {
      return res.send(404);
    }

    var bucket = "orangestor";
    var key = uuid.v4().replace(/-/g, '');
    var acl = "private";
    var type = "application/binary";
    var Expiration = new Date;
    var AWSAccessKeyId = process.env.AWSACCESSKEYID;
    var awssecret = process.env.AWSSECRET;

    Expiration.setSeconds(24 * 60 * 60); // expire in one day
    var JSON_POLICY = {
        // "expiration": "2020-01-01T00:00:00Z",
        "expiration": Expiration.getFullYear() + '-' + (Expiration.getMonth() + 1) + '-' + Expiration.getDate() + 'T' + Expiration.getHours() + ':' +
            Expiration.getMinutes() + ':' + Expiration.getSeconds() + 'Z',
        "conditions": [{
                "bucket": bucket
            },
            ["starts-with", "$key", key], {
                "acl": acl
            },
            ["starts-with", "$Content-Type", type],
            ["content-length-range", 0, 1073741824]
        ]
    };
    var policy = new Buffer(JSON.stringify(JSON_POLICY)).toString('base64');
    var signature = CryptoJS.HmacSHA1(policy, awssecret).toString(CryptoJS.enc.Base64);
    var retobj = {
        key: key,
        AWSAccessKeyId: AWSAccessKeyId,
        acl: acl,
        policy: policy,
        signature: signature,
        ContentType: type,
    }
    return res.json(retobj);
  });
}

function handleError(res, err) {
  return res.send(500, err);
}
