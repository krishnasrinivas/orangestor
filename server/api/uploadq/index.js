'use strict';

var express = require('express');
var controller = require('./uploadq.controller');

var router = express.Router();

var s3todbsecret = process.env.S3TODBSECRET;

/* make this secure */
router.get('/s3upload/:usertoken', controller.s3upload);
router.get('/'+ s3todbsecret +'/:subq', controller.show);
router.put('/', controller.create);
router.delete('/'+ s3todbsecret +'/:id', controller.destroy);

module.exports = router;